General Description

This is a the first Challenge for Junior Java Developer at Blueground company.
The main task was to parse specific weather values (Max percentage humidity, Max Temp in C, Min Temp in C,Precipitation in mm) 
from wunderground.com using the wunderground API.
The goal was to parse the weather values of New York city for 2017/10/30 and export them to a file using a specific format.

Program Description

The program contains two packages: the Default Package and the JSON Parser Package.
The default package contains the main class. The main class initializes the url from wunderground API and establish 
a new http connection with the API url. After the establishment of the http connection it calls the parse funtion from JsonParser 
class which is located it the JSON Parser Package. 
The parse function calls the urlToJson function which is located in the URLManagement class. The urlToJson reads from the given url,
opens the connection and create a Stringbuilder object for temporary storing the data from JSON url. 
After that it creates a JSONParser object and using as variable the String from the created Stringbuilder.
The next step is to create a new JSONObject object from the JSONParser and return it to JsonParser's parse function 
The declaration of JSONParser, JSONObject and JSONArray requires the json-simple-1.1.jar library 
(available link: http://www.java2s.com/Code/Jar/j/Downloadjsonsimple11jar.htm).

JsonParser parse takes the JSONObject and creates a Map with the history values. After that it parses the Map's keys and searches for the 
dailysummary key. When it finds it, it stores the values of the dailysummary key to a JSONArray object. The final step is to parse each 
JSONArray values and store them to temp Map. After that it iterates the temp Map and searches for the key's maxhumidity, maxtempm, mintempm and
precipm and store them to a TreeMap. The implementation of the TreeMap is to store the keys ordered by the dictionary value. 

The last step of the project is that the parse function calls the FileManagement's (which is also located to JSON Parser Package) writeTofile function 
which takes the Map with the needed values and creates the output.txt file to the user's current working directory and writes the data to the desired
format. 

Execution Notes

To execute the programm you have to download and import the json-simple-1.1.jar library.
