package JSONParser;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.zip.GZIPInputStream;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class JsonParser 
{    
    //the parse fuction which handles the data of the JSON object
    public static void parse(URL url) throws ParseException, FileNotFoundException, IOException 
    {
        try 
        {
            //creates a JSONObject as returned from urlToJson function
            JSONObject jsonObject = URLManagement.urlToJson(url);

            //Creates a Map with the history field of the JSON and creates a JSONArray Object
            Map history = ((Map)jsonObject.get("history"));
            JSONArray json = new JSONArray();
            
            //for each Map Entry key in history Map and if the field is dailysummary stores in the JSONArry the values
            Iterator<Map.Entry> itr1 = history.entrySet().iterator();
            while (itr1.hasNext()) 
            {
                Map.Entry pair = itr1.next();
                
                if(pair.getKey().equals("dailysummary"))
                    json = (JSONArray) pair.getValue();
            }
            
            //creates the final Map with the needed fields 
            //the Impemantation of TreeMap is for automated dictionary sorting the Key values
            Map<String, String> output = new TreeMap<String, String>();
            
            //in the JSONArray searches the keys and if the Keys are equal with the needed fields then they stored as a key-value pair in the output Map 
            Iterator itr2 = json.iterator();
            while (itr2.hasNext()) 
            {
                itr1 = ((Map) itr2.next()).entrySet().iterator();
                while (itr1.hasNext()) 
                {
                    Map.Entry pair = itr1.next();
                    if(pair.getKey().equals("maxhumidity")||pair.getKey().equals("maxtempm")||pair.getKey().equals("mintempm")||pair.getKey().equals("precipm"))
                    {    
                        output.put(pair.getKey().toString(), pair.getValue().toString());
                    }
                }
            }
            //calls the writeTofile function to write the results to a file
            FileManagement.writeTofile(output);
        } 
        catch (NullPointerException ex){}
    }
    
    
}