package JSONParser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.zip.GZIPInputStream;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class URLManagement 
{
	//the function which transforms the data from url to a String and returns a JSONObject object
    public static JSONObject urlToJson(URL url) throws ParseException 
    {
        StringBuilder sb = null;
        URLConnection urlCon;
        try 
        {
            //opens the the connection with the given json url
            urlCon = url.openConnection();
            
            //reads from the response of the http request and store the response (the json file) to a StringBuilder object 
            BufferedReader in = null;
            if (urlCon.getHeaderField("Content-Encoding") != null && urlCon.getHeaderField("Content-Encoding").equals("gzip"))
                in = new BufferedReader(new InputStreamReader(new GZIPInputStream(urlCon.getInputStream())));
            else
                in = new BufferedReader(new InputStreamReader(urlCon.getInputStream()));
            String inputLine;
            sb = new StringBuilder();

            while ((inputLine = in.readLine()) != null) {
                sb.append(inputLine);
            }
            in.close();
        } 
        catch (IOException e) {}
        
        /*creates a JSONParser object from the StringBuilder above if it is not null
        * else it creates a null JSON object
        */
        JSONParser parser = new JSONParser(); 
        if (sb != null) 
        {
            //creates a valid JSON Object from JSON parser
            JSONObject json = (JSONObject) parser.parse(sb.toString());
            return json;
        } 
        else 
        {
            //creates an empty JSON object
            JSONObject json = (JSONObject) parser.parse("");
            return json;
        }
    }
}
