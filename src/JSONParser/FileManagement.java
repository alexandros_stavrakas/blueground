package JSONParser;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Map.Entry;

public class FileManagement 
{
	//writeTofile function stores the results to a file with specific format
    public static void writeTofile(Map<String, String> mapKeyValues) throws IOException
    {
        //find the current working path and store it as a String
        Path currentRelativePath = Paths.get("");
        String filePath = currentRelativePath.toAbsolutePath().toString();
        
        //create a file in the current working path and name it as output.txt
        File file = new File(filePath+"/output.txt");
        
        //creates the file if not exists
        if (!file.exists()) 
        	file.createNewFile();
        
        //creates a File and a Buffer write to write to file 
        FileWriter fw = new FileWriter(file);
        BufferedWriter output = new BufferedWriter(fw);
        
        //write to file with specific format using the String.format function
        //the Main title of the file
        output.write(String.format("%-20s%n", "Table 2"));
        //the two columns
        output.write(String.format("%-20s %-20s%n", "Metric", "Keyword"));
        
        //parsing the TreeMap and writes the keys under the Metric column and values under th Keyword column
        for (Entry<String, String> e : mapKeyValues.entrySet()) 
        {
            String key = e.getKey();
            String value = e.getValue();
            output.write(String.format("%-20s %-20s%n", value, key));
        }
        
        //closing the file writing Buffer 
        output.close();
    }
}
