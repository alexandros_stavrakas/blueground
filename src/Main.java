import JSONParser.JsonParser;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import org.json.simple.parser.ParseException;

/**
 *
 * @author Alexandros Stavrakas
 */
public class Main 
{
    //String to construct the JSON url
    private static final String apiUrl = "http://api.wunderground.com/api/";
    private static final String apiKey = "73a4a851915e5b1e";
    private static final String historyDate = "history_20171030";
    private static final String place = "NY/New_York";
    private static final String urlString = apiUrl+apiKey+"/"+historyDate+"/q/"+place+".json";//"http://api.wunderground.com/api/73a4a851915e5b1e/history_20171030/q/NY/New_York.json";
    
    public static void main(String[] args) throws IOException, ParseException 
    {
        //Create URL object and enstablish an HTTP conection with the given url
        URL url = new URL(urlString);
        HttpURLConnection request = (HttpURLConnection) url.openConnection();
        request.connect();
        
        //Call from JsonParser object the parse function and parse the url as a variable
        JsonParser.parse(url);
    }
}
